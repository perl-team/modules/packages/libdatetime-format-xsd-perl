libdatetime-format-xsd-perl (0.4-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 09:50:00 +0100

libdatetime-format-xsd-perl (0.4-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Salvatore Bonaccorso ]
  * update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * use MetaCPAN URI (not search.cpan.org or www.cpan.org)

  [ Debian Janitor ]
  * use secure copyright file specification URI

  [ Jonas Smedegaard ]
  * simplify rules;
    stop build-depend on dh-buildinfo devscripts cdbs
  * use debhelper compatibility level 13 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * annotate test-only build-dependencies
  * declare compliance with Debian Policy 4.6.0
  * set Rules-Requires-Root: no
  * update watch file:
    + use file format 4
    + mention gbp --uscan in usage comment
    + use substitution strings
  * update copyright info:
    + use License-Grant and License-Reference
    + add lintian overrides about License-Reference field
    + license packaging as GPL-3+ (not GPL-2+)
    + stop track no longer embedded code
    + update coverage
  * use semantic newlines in long description and copyright fields

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Sep 2021 18:31:52 +0200

libdatetime-format-xsd-perl (0.2-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 23:21:19 +0100

libdatetime-format-xsd-perl (0.2-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#679134.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 26 Jun 2012 17:25:09 +0200
